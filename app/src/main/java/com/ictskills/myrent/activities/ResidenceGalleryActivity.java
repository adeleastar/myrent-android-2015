package com.ictskills.myrent.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.ictskills.myrent.R;
import com.ictskills.myrent.app.MyRentApp;
import com.ictskills.myrent.models.Portfolio;
import com.ictskills.myrent.models.Residence;

import java.util.UUID;

import static com.ictskills.android.helpers.CameraHelper.showPhoto;

public class ResidenceGalleryActivity extends Activity
{
  public static final String EXTRA_PHOTO_FILENAME = "com.ictskills.myrent.photo.filename";
  private ImageView photoView;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.residence_gallery);
    photoView = (ImageView) findViewById(R.id.residenceGalleryImage);
    getActionBar().setDisplayHomeAsUpEnabled(true);
    showPicture();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch(item.getItemId())
    {
      case android.R.id.home :  onBackPressed();
                        return true;

      default : return super.onOptionsItemSelected(item);
    }
  }
  private void showPicture()
  {
    UUID resId = (UUID)getIntent().getSerializableExtra(ResidenceFragment.EXTRA_RESIDENCE_ID);
    MyRentApp app = (MyRentApp)getApplication();
    Portfolio portfolio = app.portfolio;
    Residence residence = portfolio.getResidence(resId);
    showPhoto(this, residence, photoView);
  }
}
