package com.ictskills.myrent.app;

import com.ictskills.myrent.models.Portfolio;
import com.ictskills.myrent.models.PortfolioSerializer;

import android.app.Application;
import static com.ictskills.android.helpers.LogHelpers.info;

public class MyRentApp extends Application
{
  public Portfolio portfolio;

  private static final String FILENAME = "portfolio.json";

  @Override
  public void onCreate()
  {
    super.onCreate();
    PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
    portfolio = new Portfolio(serializer);

    info(this, "MyRent app launched");
  }
}
